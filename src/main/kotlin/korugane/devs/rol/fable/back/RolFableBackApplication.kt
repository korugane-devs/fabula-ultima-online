package korugane.devs.rol.fable.back

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RolFableBackApplication

fun main(args: Array<String>) {
	runApplication<RolFableBackApplication>(*args)
}
